package team.maci.polish

import team.maci.polish.impl.PolishNotationParser
import team.maci.polish.impl.PolishNotationSolver

fun main(args: Array<String>){
    val polishParser = PolishNotationParser()
    val polishSolver = PolishNotationSolver()
    val result = polishParser.parse("(1.1+2200)+8.9*3/3")
    val calculatedResult = polishSolver.solve(result)
    println(calculatedResult)
}