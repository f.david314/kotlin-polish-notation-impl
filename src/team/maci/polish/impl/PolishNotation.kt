package team.maci.polish.impl

import team.maci.polish.impl.operation.NumericOperation
import team.maci.polish.impl.operation.Operation
import team.maci.polish.impl.token.Token
import team.maci.polish.impl.token.factory.NumericTokenFactory
import team.maci.polish.impl.token.factory.OperatorTokenFactory
import team.maci.polish.impl.token.impl.NumericToken
import team.maci.polish.impl.token.impl.OperatorToken
import java.util.*


class PolishNotationSolver{
    fun solve(input: Stack<Token>) : Double{
        if(input.size == 0){
            throw IllegalArgumentException("Invalid input stack: The stack must contains at least one element")
        }

        val copiedInput = Stack<Token>().apply {
            addAll(input)
        }

        if(input.peek() !is NumericToken){
            input.reverse()
        }


        val outputStack = Stack<Operation>()

        while(copiedInput.size > 0){
            val targetToken = copiedInput.pop()
            targetToken.createOpreation().execute(outputStack)
        }

        if(outputStack.size != 1 || outputStack.peek() !is NumericOperation){
            throw IllegalArgumentException("Invalid input stack!")
        }

        val result = outputStack.peek() as NumericOperation
        return result.value
    }
}

class PolishNotationParser{
    private val operatorFactory = OperatorTokenFactory()
    private val numericFactory = NumericTokenFactory()


    fun parse(input: String) : Stack<Token>{
        val outputStack = Stack<Token>()
        val operatorStack = Stack<OperatorToken>()


        var currentIndex = 0
        while(currentIndex < input.length){
            val target = input[currentIndex]
            when {
                numericFactory.isNumeric(target) -> {
                    var endIndex = currentIndex
                    while(endIndex < input.length && numericFactory.isNumeric(input[endIndex])){
                        endIndex++
                    }
                    val number = input.substring(currentIndex, endIndex)
                    outputStack.push(numericFactory.createNumericToken(number))
                    currentIndex = endIndex
                }
                operatorFactory.isOperator(target) -> {
                    val operatorToken = operatorFactory.createOperatorToken(target)
                    operatorToken.executeOnStacks(outputStack, operatorStack)
                    currentIndex++
                }
                target.isWhitespace() -> {
                    //Ignore
                    currentIndex++
                }
                else -> throw IllegalArgumentException("Not supported character: $target")
            }
        }


        while(!operatorStack.isEmpty()){
            outputStack.push(operatorStack.pop())
        }

        outputStack.reverse()


        return outputStack
    }
}
