package team.maci.polish.impl.operation

import java.util.*


abstract class MathematicalOperation : Operation {
    override fun execute(stack: Stack<Operation>) {
        val right = stack.pop() as NumericOperation
        val left = stack.pop() as NumericOperation

        executeImpl(stack, left.value, right.value)
    }

    abstract fun executeImpl(stack: Stack<Operation>, left: Double, right: Double)
}

class PlusOperation : MathematicalOperation() {
    override fun executeImpl(stack: Stack<Operation>, left: Double, right: Double) {
        stack.push(NumericOperation(left + right))
    }
}

class MinusOperation : MathematicalOperation() {
    override fun executeImpl(stack: Stack<Operation>, left: Double, right: Double) {
        stack.push(NumericOperation(left - right))
    }
}

class DivideOperation : MathematicalOperation(){
    override fun executeImpl(stack: Stack<Operation>, left: Double, right: Double) {
        stack.push(NumericOperation(left / right))
    }
}

class MultipleOperation : MathematicalOperation(){
    override fun executeImpl(stack: Stack<Operation>, left: Double, right: Double) {
        stack.push(NumericOperation(left * right))
    }
}