package team.maci.polish.impl.operation

import java.util.*

class NumericOperation(val value: Double) : Operation{
    override fun execute(stack: Stack<Operation>) {
        stack.push(this)
    }

}