package team.maci.polish.impl.operation

import java.util.*

interface Operation{
    fun execute(stack: Stack<Operation>)
}