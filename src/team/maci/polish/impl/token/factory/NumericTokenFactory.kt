package team.maci.polish.impl.token.factory

import team.maci.polish.impl.token.impl.NumericToken

class NumericTokenFactory{
    fun isNumeric(input: Char) : Boolean{
        return input.isDigit() || input == '.'
    }

    fun createNumericToken(input: String) : NumericToken {
        return NumericToken(input)
    }
}