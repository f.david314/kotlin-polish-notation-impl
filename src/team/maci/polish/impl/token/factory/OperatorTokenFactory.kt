package team.maci.polish.impl.token.factory

import team.maci.polish.impl.token.impl.*


class OperatorTokenFactory {
    private val operatorTokenFactories: Map<Char, () -> OperatorToken> = mapOf(
            '+' to {
                PlusOperatorToken()
            },
            '-' to {
                MinusOperatorToken()
            },
            '/' to {
                DivideOperatorToken()
            },
            '*' to {
                MultipleOperatorToken()
            },
            '(' to {
                StartBracketOperatorToken()
            },
            ')' to {
                EndBracketOpertorToken()
            }
    )

    fun isOperator(input: Char) : Boolean{
        return operatorTokenFactories.containsKey(input)
    }

    fun createOperatorToken(input: Char) : OperatorToken {
        return operatorTokenFactories[input]?.invoke() ?: throw IllegalArgumentException("We don't have operator token factory for the following character: $input")
    }
}