package team.maci.polish.impl.token.impl

import team.maci.polish.impl.operation.NumericOperation
import team.maci.polish.impl.operation.Operation
import team.maci.polish.impl.token.Token

class NumericToken(val value: String) : Token() {
    override fun createOpreation(): Operation {
        return NumericOperation(value.toDouble())
    }
}