package team.maci.polish.impl.token.impl

import team.maci.polish.impl.operation.*
import team.maci.polish.impl.token.Token
import java.util.*

abstract class OperatorToken : Token() {
    abstract fun getPrecedence(): Int

    open fun executeOnStacks(outputStack: Stack<Token>, operatorStack: Stack<OperatorToken>){
        while(!operatorStack.empty() && operatorStack.peek().getPrecedence() > getPrecedence()){
            val poppedOperatorToken = operatorStack.pop()
            outputStack.push(poppedOperatorToken)
        }

        operatorStack.push(this)
    }
}

class PlusOperatorToken : OperatorToken() {
    override fun createOpreation() = PlusOperation()

    override fun getPrecedence() = 1
}

class MinusOperatorToken : OperatorToken() {
    override fun getPrecedence() = 1

    override fun createOpreation() = MinusOperation()
}

class DivideOperatorToken : OperatorToken() {
    override fun getPrecedence() = 2
    override fun createOpreation() = DivideOperation()
}

class MultipleOperatorToken : OperatorToken(){
    override fun getPrecedence(): Int = 2

    override fun createOpreation() = MultipleOperation()
}

class StartBracketOperatorToken : OperatorToken() {
    override fun getPrecedence() = 0

    override fun createOpreation() = throw IllegalStateException("Stack bracket operator token doesn't have any numeric operation")
}

class EndBracketOpertorToken : OperatorToken(){
    override fun getPrecedence(): Int = 0

    override fun createOpreation() = throw IllegalStateException("End bracket operator token doesn't have any numeric operation")

    override fun executeOnStacks(outputStack: Stack<Token>, operatorStack: Stack<OperatorToken>) {
        while(!operatorStack.empty() && operatorStack.peek() !is StartBracketOperatorToken){
            val poppedOperatorToken = operatorStack.pop()
            outputStack.push(poppedOperatorToken)
        }

        if(operatorStack.empty()){
            throw IllegalStateException("End bracket without start bracket is not allowed")
        }
        operatorStack.pop()
    }

}